/*
 * main.c
 *
 *  Created on: 07.03.2016
 *      Author: Michael Lipp
 */

/*#################################################################################################
	Fuer	: Pedalerie RT10
	Version	: 1.0
	autor	: Michael Lipp
	License	: GNU General Public License
	Hardware: AT90CAN128, 16MHz,
	Aufgabe :
//##################################################################################################
	kein watchdog moeglich ... sonst controller bei softwarereset im Bootloadermodus
//################################################################################################*/


#include "main.h"

uint8_t CAN_counter_Status 	= 0;
uint8_t CAN_counter_Data	= 0;

void initialisierung(void)
{
	wdt_disable();													//watchdog disable
	cli();															//disable interrupts

//I�C

	i2c_init();			//initialisiere I2C-Bus

//#######################################8 Bit Timer0 f�r Mittelwertfilter
	TCCR0A  	/*|= _BV(CS00)*/ |=_BV(CS01);						// 8bit -  Prescaler 8 - z�hler 155-->100 schritte --> 0,05ms
	TCNT0  		= 155;												// Timer 155 setzen z�hlt durch, overflow nach 0,05 ms
	TIMSK0  	|= _BV(TOIE0);

// Timer 2 for controller-internal operations
	TCCR2A		= (1<<CS22);										// Prescaler 64
	TCNT2		= 5;												// 8-bit Timer, also 255 - 5 (TCNT = 5) Schritte -> 1000 Schritte pro Sekunde -> 1,0ms!
	TIMSK2		|= _BV(TOIE2);

//####################################### enable interrupts
	_delay_ms(100);
		sei();								// enable interrupts

// CAN-Empfang mit Interrupt-gest�tzem Einlesen der Nachrichten

	CAN_init(1000, RX);												// Bus mit Baudrate von 1000kBaud initialisieren und Receive-Interrupts aktivieren
	define_MObs();													// Message Objekte definieren und aktivieren


//Definition of Controller-Pins

	//Inputs
		DDRE &= ~(1 << PE3);		//BOTS

		DDRC &= ~(1 << PC0);		//Gas 1
		DDRC &= ~(1 << PC1);		//Gas 2
		DDRC &= ~(1 << PC2);		//Bremse
		DDRC &= ~(1 << PC3);		//Waagebalken
		DDRC &= ~(1 << PC4);		//BSPD_RAW_Sig
		DDRC &= ~(1 << PC5);		//BSPD_Sig

	//Pull-Ups
		PORTE |= (1 << PE3);		//BOTS
		PORTC |= (1 << PC5);		//BSPD_Sig

}

void Messen_ADC(void)
{
	uint8_t		high,low = 0;						// high und Low auslese Variable f�r uniq

	if (i2c_start(Adr_MAX127 + I2C_WRITE) == 0)		// Safe_System Abfrage ob Maxim erreichbar, liefert wenn erreichbar Wert 0 zur�ck
		{
			i2c_start_wait(Adr_MAX127 + I2C_WRITE);	// Rufe ADC �ber seine Adresse mit Schreib-Befehl auf
			i2c_write(Adr_Gas_1);					// Channel Gas1 aufrufen
			i2c_stop();								// Bus wieder freigeben

			i2c_rep_start(Adr_MAX127 + I2C_READ);	// Rufe wieder ADC �ber seine Adresse auf, aber mit Lese-Befehl
			low		= i2c_read(1);					// Lese 'low'-Wert ein
			high	= i2c_read(0);					// Lese 'high'-Wert ein
			i2c_stop();								// Bus wieder freigeben

			RawValue.translation.gas1 = (((low << 8)| high) >> 4) & 0x0FFF;	// ADC-Rohwert in entsprechende Variable schreiben
		}

	if (i2c_start(Adr_MAX127 + I2C_WRITE) == 0)		// Safe_System Abfrage ob Maxim erreichbar, liefert wenn erreichbar Wert 0 zur�ck
		{
			i2c_start_wait(Adr_MAX127 + I2C_WRITE);	// Rufe ADC �ber seine Adresse mit Schreib-Befehl auf
			i2c_write(Adr_Gas_2);					// Channel Gas2 aufrufen
			i2c_stop();								// Bus wieder freigeben

			i2c_rep_start(Adr_MAX127 + I2C_READ);	// Rufe wieder ADC �ber seine Adresse auf, aber mit Lese-Befehl
			low		= i2c_read(1);					// Lese 'low'-Wert ein
			high	= i2c_read(0);					// Lese 'high'-Wert ein
			i2c_stop();								// Bus wieder freigeben

			RawValue.translation.gas2 = (((low << 8)| high) >> 4) & 0x0FFF;	// ADC-Rohwert in entsprechende Variable schreiben
		}

	if (i2c_start(Adr_MAX127 + I2C_WRITE) == 0)		// Safe_System Abfrage ob Maxim erreichbar, liefert wenn erreichbar Wert 0 zur�ck
		{
			i2c_start_wait(Adr_MAX127 + I2C_WRITE);	// Rufe ADC �ber seine Adresse mit Schreib-Befehl auf
			i2c_write(Adr_Bremse);					// Channel Bremse aufrufen
			i2c_stop();								// Bus wieder freigeben

			i2c_rep_start(Adr_MAX127 + I2C_READ);	// Rufe wieder ADC �ber seine Adresse auf, aber mit Lese-Befehl
			low		= i2c_read(1);					// Lese 'low'-Wert ein
			high	= i2c_read(0);					// Lese 'high'-Wert ein
			i2c_stop();								// Bus wieder freigeben

			RawValue.translation.bremse = (((low << 8)| high) >> 4) & 0x0FFF;	// ADC-Rohwert in entsprechende Variable schreiben
		}

	if (i2c_start(Adr_MAX127 + I2C_WRITE) == 0)		// Safe_System Abfrage ob Maxim erreichbar, liefert wenn erreichbar Wert 0 zur�ck
		{
			i2c_start_wait(Adr_MAX127 + I2C_WRITE);	// Rufe ADC �ber seine Adresse mit Schreib-Befehl auf
			i2c_write(Adr_Waagebalken);				// Channel Waagebalken aufrufen
			i2c_stop();								// Bus wieder freigeben

			i2c_rep_start(Adr_MAX127 + I2C_READ);	// Rufe wieder ADC �ber seine Adresse auf, aber mit Lese-Befehl
			low		= i2c_read(1);					// Lese 'low'-Wert ein
			high	= i2c_read(0);					// Lese 'high'-Wert ein
			i2c_stop();								// Bus wieder freigeben

			RawValue.translation.waage = (((low << 8)| high) >> 4) & 0x0FFF;	// ADC-Rohwert in entsprechende Variable schreiben
		}
}

void SendCAN_Pedalerie(void)
{
// Sende Pedalerie-ID
	if(CAN_counter_Status == 1)
	{
		can_t MOb_Pedalerie =
		{
			.idm			= 0xFFFFFFFF,
			.id				= CAN_ID_PEDAL,
			.length			= 0
		};
		CAN_enableMOB	(13, RECEIVE_DATA, MOb_Pedalerie);
		CAN_sendData	(13, MOb_Pedalerie);
		CAN_disableMOB	(13);
	}

// Sende Pedalerie-Status-Nachricht
	if(CAN_counter_Status == 2)
	{
		can_t MOb_Ped_Status =
		{
			.idm			= 0xFFFFFFFF,
			.id				= CAN_ID_PED_STAT,
			.length			= 3
		};

		for(uint8_t i = 0; i < 3 ; i++)
		{
			MOb_Ped_Status.data[i]			= Ped_Status.data[i];
		}
		CAN_enableMOB	(13, RECEIVE_DATA, MOb_Ped_Status);
		CAN_sendData	(13, MOb_Ped_Status);
		CAN_disableMOB	(13);


		CAN_counter_Status = 0;
	}

	if(Ped_Status.translation.pedal_cal_off)
	{
		can_t MOb_Cal_Off =
		{
			.idm			= 0xFFFFFFFF,
			.id				= CAN_ID_CAL_OFF,
			.length			= 0
		};
		CAN_enableMOB	(13, RECEIVE_DATA, MOb_Cal_Off);
		CAN_sendData	(13, MOb_Cal_Off);
		CAN_disableMOB	(13);

		Ped_Status.translation.pedal_cal_off = 0;
	}
}

void SendCAN_Data(void)
{
	//Sende Daten Gaspedal
if(CAN_counter_Data == 1)
	{
		can_t MOb_Ped_Throttle =
		{
				.idm			= 0xFFFFFFFF,
				.id				= CAN_ID_THR_PEDAL,
				.length			= 6
		};

		for(uint8_t i = 0; i < MOb_Ped_Throttle.length ; i++)
		{
			MOb_Ped_Throttle.data[i]	= Thr_Pedal.data[i];
		}
		CAN_enableMOB	(14, RECEIVE_DATA, MOb_Ped_Throttle);
		CAN_sendData	(14, MOb_Ped_Throttle);
		CAN_disableMOB	(14);
	}

	//Sende Daten Bremspedal
if(CAN_counter_Data == 2)
	{
		can_t MOb_Ped_Brake =
		{
				.idm			= 0xFFFFFFFF,
				.id				= CAN_ID_BR_PEDAL,
				.length			= 2
		};

		for(uint8_t i = 0; i < MOb_Ped_Brake.length ; i++)
		{
			MOb_Ped_Brake.data[i]	= Br_Pedal.data[i];
		}
		CAN_enableMOB	(14, RECEIVE_DATA, MOb_Ped_Brake);
		CAN_sendData	(14, MOb_Ped_Brake);
		CAN_disableMOB	(14);
	}

	//Sende Daten Waagebalken
if(CAN_counter_Data == 3)
	{
		can_t MOb_Ped_Br_Adj =
		{
				.idm			= 0xFFFFFFFF,
				.id				= CAN_ID_BR_ADJ,
				.length			= 2
		};

		for(uint8_t i = 0; i < MOb_Ped_Br_Adj.length ; i++)
		{
			MOb_Ped_Br_Adj.data[i]	= Br_Adj.data[i];
		}
		CAN_enableMOB	(14, RECEIVE_DATA, MOb_Ped_Br_Adj);
		CAN_sendData	(14, MOb_Ped_Br_Adj);
		CAN_disableMOB	(14);
		CAN_counter_Data = 0;
	}
}

void Plausibility_Check(void)
{
//	for(uint8_t i = 0; i < 3; i++)
//	{
//		Signal.data[i] = - RawValue.data[i] + Average.data[i] - 20;
//	}

	//Offset einbeziehen f�r die Plausibilisierung

	Signal.translation.gas1 	= - RawValue.translation.gas1 	+ Average.translation.gas1		- 50;
	Signal.translation.gas2 	= - RawValue.translation.gas2 	+ Average.translation.gas2		- 50;
	Signal.translation.bremse 	= - RawValue.translation.bremse + Average.translation.bremse	- 50;
	Signal.translation.waage 	= - RawValue.translation.waage 	+ Average.translation.waage		- 50;

	// unsinnige Werte l�schen

	for(uint8_t i = 0; i < 4 ; i++)
	{
		if((Signal.data[i] <= 0 || Signal.data[i] >= 4096))
			Signal.data[i] = 0;
	}



	// Gaspedalsensorwerte mitteln und in 'Egas' schreiben

	if(RawValue.translation.gas1 < 500 || RawValue.translation.gas2 < 500)
		Thr_Pedal.translation.egas = 0;

	if(Signal.translation.gas1 > 0 && Signal.translation.gas2 > 0)
		{
			Thr_Pedal.translation.egas = ((Signal.translation.gas1 + Signal.translation.gas2) /2);
		}
	else
		Thr_Pedal.translation.egas = 0;

	if (Thr_Pedal.translation.egas > 2200)
		Thr_Pedal.translation.egas = 0;

	// Plausibilisierung zwischen Gas- und Bremspedal pr�fen

		// Gas gesperrt, wenn Bremse gedr�ckt und Gaspedal mehr als 25% gedr�ckt wird

	if((Signal.translation.bremse > Min_Brake) && (Thr_Pedal.translation.egas > Max_Egas))
		{
			Thr_Pedal.translation.egas = 0;
			Ped_Status.translation.br_gas_inplau = 1;
		}

		// Gas solange gesperrt, bis Gaspedal weniger als 5% gedr�ckt wird

	else if((Ped_Status.translation.br_gas_inplau == 1) && (Thr_Pedal.translation.egas >= Min_Egas))
		{
			Thr_Pedal.translation.egas = 0;
			Ped_Status.translation.br_gas_inplau = 1;
		}

		// Status-Bit zur�cksetzen, wenn Gas weniger als 5% gedr�ckt wird

	else if(/*(Ped_Status.translation.br_gas_inplau == 1) && */(Thr_Pedal.translation.egas < Min_Egas))
		{
			Ped_Status.translation.br_gas_inplau = 0;
		}

	// Egas Null setzen, wenn das BSPD-Bit gesetzt ist, damit das BSPD wirklich nur im Fehlerfall ausl�sen kann

	if(Ped_Status.translation.bspd == 1)
		Thr_Pedal.translation.egas = 0;

	// Plausibilisierung zwischen beiden Gaspedalsensoren, Differenz darf nicht gr��er als 10% sein

	float diff_gas = abs(Signal.translation.gas1 - Signal.translation.gas2);
	float diff_gas_max = ROUND((0.1 * ((Signal.translation.gas1 + Signal.translation.gas2)/2)));

	if(Thr_Pedal.translation.egas > 1000)
	{
		if(diff_gas > diff_gas_max)
		{
			Thr_Pedal.translation.egas = 0;
			Ped_Status.translation.gas_inplau = 1;
		}
	}
	else
	{
		if(diff_gas > 100)
		{
			Thr_Pedal.translation.egas = 0;
			Ped_Status.translation.gas_inplau = 1;
		}
		else
			Ped_Status.translation.gas_inplau = 0;
	}

	if(Signal.translation.bremse> 1500)
		Signal.translation.bremse = 0;

	//�bersetze Daten in die Byteorder-Structs

	Thr_Pedal.translation.gas1 	= Signal.translation.gas1;
	Thr_Pedal.translation.gas2 	= Signal.translation.gas2;
	Br_Pedal.translation.value 	= Signal.translation.bremse;
	Br_Adj.translation.value	= Signal.translation.waage;

//	Thr_Pedal.translation.gas1 	= RawValue.translation.gas1;
//	Thr_Pedal.translation.gas2 	= RawValue.translation.gas2;
//	Br_Pedal.translation.value 	= RawValue.translation.bremse;
//	Br_Adj.translation.value	= RawValue.translation.waage;
}

void Offset(void)
{
	Messen_ADC();
	for(uint8_t i = 0; i < 4 ; i++)
	{
		Average.data[i] = RawValue.data[i];
	}
}

void Kalibrieren(uint16_t duration)
{
	uint16_t cal_counter = 0;
	memset(&Average.data,0,5);//sizeof(&Average.data));	// L�sche alle Werte aus dem Array f�r die Kalibrierungswerte

	if(duration > 20)									// Die Gr��e des Datentyps beschr�nkt die Anzahl der Schleifendurchl�ufe auf (16bit / (10bit*0,8) = 20)
		duration = 20;

	while(duration > 0)
	{
		Offset();
		duration--;
		cal_counter++;
		_delay_ms(5);
	}

	for(uint8_t i = 0; i < 4 ; i++)
	{
		Average.data[i] /= cal_counter;
	}

	Ped_Status.translation.pedal_cal_off = 1;
	Ped_Status.translation.calibration_counter = cal_counter;
}

void Mittelwertfilter(void)
{
	if(MeanFilter.counter < Iteration)
	{
		for(uint8_t i = 0; i < 4 ; i++)//(sizeof(&MeanFilter.data)-1) ; i++)
		{
			MeanFilter.data[i] += RawValue.data[i];
		}
		MeanFilter.counter++;
	}
	else if(MeanFilter.counter >= Iteration)
	{
		for(uint8_t i = 0; i < 4 ; i++)//(sizeof(&MeanFilter.data)-1) ; i++)
		{
			Signal.data[i] = (MeanFilter.data[i] / MeanFilter.counter);
		}

		MeanFilter.counter = 0;
		memset(&MeanFilter, 0, 10);
	}
}

ISR(SIG_OVERFLOW0)
{
	TCNT0 		= 155;
	timer_meanfilter--;		if (timer_meanfilter 		<=0)	{timer_meanfilter		= 0;}
}

ISR(SIG_OVERFLOW2)
{
	TCNT2  		= 5;			 					// Timer 2, overflow nach 1 ms

	timer_sendCAN--;		if (timer_sendCAN			<=0)	{timer_sendCAN			= 0;}
	timer_empfCAN--;		if (timer_empfCAN			<=0)	{timer_empfCAN			= 0;}
	timer_sendID--;			if (timer_sendID			<=0)	{timer_sendID			= 0;}
	timer_messen--;			if (timer_messen			<=0)	{timer_messen			= 0;}
}

int main(void)
{
	initialisierung();
	_delay_ms(100);

	MeanFilter.counter 			= 0;

	Offset();

	while(1)
	{
		if(timer_meanfilter <= 0)
			Messen_ADC(),
			Mittelwertfilter(),
			timer_meanfilter = 10;

		if(timer_sendCAN <= 0)
			CAN_counter_Data++,
			Plausibility_Check(),
			SendCAN_Data(),
			//Ped_Status.MeanFilterready = 0,
			timer_sendCAN = 3;

		if(BSPD_is_low)
			Ped_Status.translation.bspd = 1,
			Thr_Pedal.translation.egas = 0;
		else
			Ped_Status.translation.bspd = 0;

		if(BOTS_is_low)
			Ped_Status.translation.bots = 1,
			Thr_Pedal.translation.egas = 0;
		else
			Ped_Status.translation.bots = 0;

		if(timer_sendID <= 0)
			CAN_counter_Status++,
			timer_sendID = 50,
			SendCAN_Pedalerie();

		if(RT10_status.translation.PedCal_active)
			Kalibrieren(15);
		else
			Ped_Status.translation.pedal_cal_off = 0;
	}

	return 0;
}
