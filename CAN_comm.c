/*
 * CAN.c
 *
 *  Created on: 05.02.2016
 *      Author: Michael Lipp
 */

#include "main.h"

void define_MObs(void)
{
can_t MOb_Dash_status =								// Message_Object f�r Dashboard_Status
{
		.idm 	= 0b11111111111,
		.id		= 640
};

can_t MOb_Calibration_On =							// Message_Object Pedalkalibrierung aktiv
{
		.idm 	= 0b11111111111,
		.id		= 651
};

CAN_enableMOB(MOb_Nr_Dash_status, 	RECEIVE_DATA, 	MOb_Dash_status);
CAN_enableMOB(MOb_Nr_Cal_On, 		RECEIVE_DATA, 	MOb_Calibration_On);
}

void receive_CAN_interrupt(can_t msg)
{
	switch(msg.id)
	{
		case(CAN_ID_DB_STAT):
			for(uint8_t i = 0; i < 5; i++)
				{
					RT10_status.data[i]				= msg.data[i];
				}
			break;

		case(CAN_ID_CAL_ON):
				RT10_status.translation.PedCal_active = 1;
			break;
		default:
			break;
	}
}

SIGNAL(SIG_CAN_INTERRUPT1){
	uint8_t		save_canpage;
 	static		can_t message;


	// Aktuelle CANPAGE sichern
 	save_canpage	= CANPAGE;

    // Index des MOB ermitteln, der den Interrupt ausgel�st hat
	uint8_t mob 	= CAN_getMOBInterrupt();

	// Falls es kein g�ltiges MOB war abbrechen
	if(mob == NOMOB){
		return;
	}

	// Objekt das den Interrupt ausgel�st hat holen
	CAN_getMOB(mob);

	// Daten des MOBs aus CANMSG auslesen
	message			= CAN_getData();

	// Id der Nachricht holen
	message.id		= CAN_getID();

	// Inhalt der Nachricht in entsprechende, Code-interne Variablen schreiben
	receive_CAN_interrupt(message);

	// RXOK-Flag l�schen
	clearbit(CANSTMOB, RXOK);

	// MOB auf Empfang und CAN 2.0B Standard setzen
	CAN_setMode(RECEIVE_DATA);

    // CANPAGE wiederherstellen
	CANPAGE		= save_canpage;
}


