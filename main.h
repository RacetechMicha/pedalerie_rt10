/*
 * main.h
 *
 *  Created on: 07.03.2016
 *      Author: Michael Lipp
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>
#include <avr/wdt.h>

#include "can_ID.h"
#include "can.h"
#include "i2cmaster.h"
#include "uart.h"
#include "utils.h"


#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define sbi(ADDRESS,BIT) 	((ADDRESS) |= (1<<(BIT)))	// set Bit
#define cbi(ADDRESS,BIT) 	((ADDRESS) &= ~(1<<(BIT)))	// clear Bit
#define	toggle(ADDRESS,BIT)	((ADDRESS) ^= (1<<BIT))		// Bit umschalten

#define	bis(ADDRESS,BIT)	(ADDRESS & (1<<BIT))		// bit is set?
#define	bic(ADDRESS,BIT)	(!(ADDRESS & (1<<BIT)))		// bit is clear?

#ifndef	TRUE
	#define	TRUE	(1==1)
#elif !TRUE
	#error	fehlerhafte Definition fuer TRUE
#endif

#ifndef FALSE
	#define	FALSE	(1!=1)
#elif FALSE
	#error	fehlerhafte Definition fuer FALSE
#endif


#define ROUND(x)    ((unsigned) ((x) + .5))				// runde eine positive Zahl

volatile static	int16_t		timer_sendID			= 50,
							timer_sendCAN			= 200,
							timer_empfCAN			= 150,
							timer_meanfilter		= 20,
							timer_messen			= 10;


//Adressdefinitionen f�r I2C-Slaves

#define Adr_MAX127			0b01010000 					//last '0' selects a write condition

#define Adr_Gas_1			0b10000000					//CH0 @ MAX127
#define Adr_Gas_2			0b10010000					//CH1 @ MAX127
#define Adr_Bremse			0b10100000					//CH2 @ MAX127
#define Adr_Waagebalken		0b10110000					//CH3 @ MAX127

//Grenzwerte f�r die Plausibilisierung
#define Min_Egas			85
#define Max_Egas			340
#define Min_Brake			200

#define Iteration 			15

//Makro f�r Signalevaluierung
#define BSPD_is_low		!(PINC & (1<<PINC5))
#define BOTS_is_low		!(PINE & (1<<PINE3))

//Funktionsprototypen

void Kalibrieren(uint16_t duration);
void Messen_ADC(void);
void Plausibility_Check(void);
void SendCAN_Pedalerie(void);
void SendCAN_Data(void);
void Mittelwertfilter(void);
void Offset(void);

//Structs f�r die CAN-Kommunikation

union Thr_Pedal
{
	struct byteorder_Thr_Pedal
	{
		uint16_t egas		: 16;
		uint16_t gas1		: 16;
		uint16_t gas2		: 16;
	}translation;
	uint8_t data[6];
}Thr_Pedal;

union Br_Pedal
{
	struct byteorder_Br_Pedal
	{
		uint16_t value		: 16;
	}translation;
	uint8_t data[2];
}Br_Pedal;

union Br_Adj
{
	struct byteorder_Br_Adj
	{
		uint16_t value		: 16;
	}translation;
	uint8_t data[2];
}Br_Adj;

union Ped_Status
{
	struct byteorder_Ped_Status
	{
		uint8_t 	bspd				: 1;
		uint8_t 	bots				: 1;
		uint8_t 	gas_inplau			: 1;
		uint8_t 	br_gas_inplau		: 1;
		uint8_t 	tor_enc_cal			: 1;
		uint8_t 	pedal_cal_off		: 1;
		uint16_t 	calibration_counter :16;
	}translation;
	uint8_t data[3];

	uint8_t MeanFilterready;
}Ped_Status;

//Structs f�r die code-interne Kommunikation

typedef union Data
{
	struct values
	{
		uint16_t gas1	:16;
		uint16_t gas2	:16;
		uint16_t bremse	:16;
		uint16_t waage	:16;
		uint16_t egas	:16;
	}translation;
	uint16_t data[5];

	uint16_t counter;
}Ped_Data;

Ped_Data	Average;
Ped_Data	MeanFilter;
Ped_Data	RawValue;
Ped_Data	Signal;

union vehicle_status
{
	struct
	{
		uint8_t				Kl50_active				:1;
		uint8_t				Inv_Res_active			:1;
		uint8_t				VDCU_Res_active			:1;
		uint8_t				PedCal_active			:1;
		uint8_t				SuspCal_active			:1;
		uint8_t				Reserve_active			:1;

		uint8_t				DRS_active				:1;
		uint8_t				SoC_Mode_active			:1;

		uint8_t				IMD_failure				:1;
		uint8_t				BMS_failure				:1;
		uint8_t				BSPD_failure			:1;
		uint8_t				IS_failure				:1;
		uint8_t				SBD_failure				:1;
		uint8_t				SBl_failure				:1;
		uint8_t				SBr_failure				:1;
		uint8_t				BOTS_failure			:1;
		uint8_t				BMS_Temp_failure		:1;
		uint8_t				BMS_UV_failure			:1;
		uint8_t				BMS_OV_failure			:1;

		uint8_t				RT10_SoC				:8;

		uint8_t				Reduce_SC_active		:1;
		uint8_t				Raise_SC_active			:1;
		uint8_t				Reduce_TV_active		:1;
		uint8_t				Raise_TV_active			:1;
		uint8_t				AT90_in5_active			:1;
		uint8_t				AT90_in6_active			:1;
	}translation;
	uint8_t					data[5];

}RT10_status;

#endif /* MAIN_H_ */
