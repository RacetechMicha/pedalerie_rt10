/*
 * can_ID.h
 *
 *  Created on: 10.05.2015
 *      Author: Michael
 */

#ifndef CAN_ID_H_
#define CAN_ID_H_

//Ausgehende Nachrichten Pedalerie

#define CAN_ID_PEDAL		300
#define CAN_ID_THR_PEDAL	301
#define CAN_ID_BR_PEDAL		302
#define CAN_ID_BR_ADJ		303
#define CAN_ID_PED_STAT		310
#define CAN_ID_CAL_OFF		351

//Eingehende Nachrichten Pedalerie

#define CAN_ID_DB_STAT		640
#define CAN_ID_CAL_ON		651

enum CAN_Message_Objects
{
	MOb_Nr_Dash_status		= 0,
	MOb_Nr_Cal_On			= 1,
};

//Funktionsprototypen

void send_CAN_ID (void);
void send_CAN_Status(void);
void define_MObs(void);
//void receive_CAN_interrupt(can_t msg);


#endif /* CAN_ID_H_ */
